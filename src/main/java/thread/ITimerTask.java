package thread;

import java.util.concurrent.ScheduledFuture;

public interface ITimerTask extends Runnable,ITask
{
	
	public void setFuture(ScheduledFuture<?> future);
	
	public boolean isOver();

	public long getIntervalMill();

}
