package thread;

/**
 * 消息处理器
 */
public interface IMessageProcessor {
	/**
	 * 启动消息处理器
	 */
	public void start();

	/**
	 * 停止消息处理器
	 */
	public void stop();

	/**
	 * 向消息队列投递消息
	 * 
	 * @param msg
	 */
	public void put(ITask msg);
	
	/**
	 * 向消息队列投递定时任务
	 */
	public void putTimerTask(ITimerTask timerTask);
	

	/**
	 * 判断队列是否已经达到上限了
	 * @return
	 */
	public boolean isFull();
}