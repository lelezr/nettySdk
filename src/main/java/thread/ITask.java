package thread;

public interface ITask{
	/**执行业务**/
	public void execute() throws Throwable;
	
	/**获得线程名字**/
	String getExecuteName();
	
}
