package code;

import common.EnginService;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

/**
 * channel filter抽象类
 * @author King
 *
 */
public abstract class AbsChannelInitializer extends ChannelInitializer<SocketChannel>
{
	
	protected EnginService service;
	
	public void setEnginService(EnginService service)
	{
		this.service = service;
	}
}
