package common;

public interface IHandler{
	public void handler(Message msg) throws Throwable;
	
	/**
	 * 初始化消息体对象
	 * @return
	 */
	public abstract Object initBodyClass();
	
	/**获得线程名字**/
	public String getExecuteName();
	
}
