package common;

import code.IHandlerManager;
import code.IMessageManager;
import handler.ClientHeartBeatHandler;
import handler.ServerHeartBeatHandler;
import thread.BaseDisruptorExecutor;

public class EnginService
{
	/**消息体管理类 获得消息体用**/
	protected IMessageManager messageManager;
	/**业务处理类管理类**/
	protected IHandlerManager handlerManager;
	/**业务线程服务**/
	protected BaseDisruptorExecutor threadService;
	/**channel管理类**/
	protected ChannelManager channelManager;

	public void init()
	{
		handlerManager.addHandler(Message.M_ID, Message.H_SEND_HEART_BEAT, new ServerHeartBeatHandler());	
		handlerManager.addHandler(Message.M_ID, Message.H_RECIEVE_HEART_BEAT, new ClientHeartBeatHandler());	
	}
	
	
	@SuppressWarnings("unchecked")
	public <T extends IMessageManager> T getMessageManager() {
		return (T)messageManager;
	}

	@SuppressWarnings("unchecked")
	public <T extends IHandlerManager> T getHandlerManager() {
		return (T)handlerManager;
	}

	@SuppressWarnings("unchecked")
	public <T extends ChannelManager> T getChannelManager() {
		return (T)channelManager;
	}

	public void setChannelManager(ChannelManager channelManager) {
		this.channelManager = channelManager;
	}

	@SuppressWarnings("unchecked")
	public <T extends BaseDisruptorExecutor> T  getThreadService() {
		return (T)threadService;
	}

	public void setThreadService(BaseDisruptorExecutor threadService) {
		this.threadService = threadService;
	}


	public void setMessageManager(IMessageManager messageManager) {
		this.messageManager = messageManager;
	}


	public void setHandlerManager(IHandlerManager handlerManager) {
		this.handlerManager = handlerManager;
	}


	public void shutdown()
	{
		this.threadService.stop();
	}
}
